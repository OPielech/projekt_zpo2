package edu.ib.planner.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.ActionBar
import edu.ib.planner.R
import edu.ib.planner.activities.excercises.BackExcercisesActivities
import edu.ib.planner.activities.excercises.ChestExcercisesActivity

class ChestOrBackActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chest_or_back)

        val actionBar: ActionBar? = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
    }

    fun chestButtonOnClick(view: View) {
        val intent = Intent(this, ChestExcercisesActivity::class.java)
        startActivity(intent)
    }
    fun backButtonOnClick(view: View) {
        val intent = Intent(this, BackExcercisesActivities::class.java)
        startActivity(intent)
    }
}
