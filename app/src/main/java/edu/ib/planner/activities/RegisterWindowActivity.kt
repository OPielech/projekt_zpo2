package edu.ib.planner.activities

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import edu.ib.planner.R


class RegisterWindowActivity : AppCompatActivity() {


    private var mAuth: FirebaseAuth? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_window)
        mAuth = FirebaseAuth.getInstance()

    }

        fun onClickLogin(view: View?) {

        val edtEmail = findViewById<View>(R.id.username) as EditText
        val edtPassword = findViewById<View>(R.id.password) as EditText
        val editRepeatPassword = findViewById<View>(R.id.repeatPassword) as EditText

        val email = edtEmail.text.toString()
        val password = edtPassword.text.toString()
        val repeatPassword = editRepeatPassword.text.toString()


        if (TextUtils.isEmpty(email)) {
            Toast.makeText(
                applicationContext,
                "Podaj swój email!!",
                Toast.LENGTH_LONG
            )
                .show()
            return
        }
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(
                applicationContext,
                "Podaj swoje hasło!!",
                Toast.LENGTH_LONG
            )
                .show()
            return
        }

            if(password != repeatPassword){
                Toast.makeText(
                    applicationContext,
                    "Hasła różnią się od siebie!!",
                    Toast.LENGTH_LONG
                )
                    .show()
                return

            }


        mAuth
            ?.createUserWithEmailAndPassword(email, password)
            ?.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Toast.makeText(
                        applicationContext,
                        "Rejestracja powiodła się !",
                        Toast.LENGTH_LONG
                    )
                        .show()


                    val intent = Intent(
                        this@RegisterWindowActivity,
                        MainWindowActivity::class.java
                    )
                    startActivity(intent)
                } else {


                    Toast.makeText(
                        applicationContext, "Nie udało się utworzyć konta!!"
                                + " Spróbuj później",
                        Toast.LENGTH_LONG
                    )
                        .show()

                }
            }
    }

    fun onClickBack(view: View) {

        val intent = Intent(this, StartWindowActivity::class.java)
        startActivity(intent)
    }




}