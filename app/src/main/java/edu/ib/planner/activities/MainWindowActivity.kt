package edu.ib.planner.activities

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import edu.ib.planner.R
import edu.ib.planner.activities.excercises.*

class MainWindowActivity : AppCompatActivity() {

    private lateinit var chestOrBackButton: Button
    private lateinit var bellyButton: Button
    private lateinit var leftForearmButton: Button
    private lateinit var rightForearmButton: Button
    private lateinit var leftArmButton: Button
    private lateinit var rightArmButton: Button
    private lateinit var leftShoulderButton: Button
    private lateinit var rightShoulderButton: Button
    private lateinit var leftThighButton: Button
    private lateinit var rightThighButton: Button
    private lateinit var leftCalfButton: Button
    private lateinit var rightCalfButton: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_window)

        hideButtons()
    }

    fun workoutPlanButtonOnClick(view: View) {
        val intent = Intent(this, WorkoutPlanActivity::class.java)
        startActivity(intent)
    }

    fun bellyButtonOnClick(view: View) {
        val intent = Intent(this, BellyExcercisesActivity::class.java)
        startActivity(intent)
    }

    fun chestOrBackButtonOnClick(view: View) {
        val intent = Intent (this, ChestOrBackActivity::class.java)
        startActivity(intent)
    }//end of chestOrBackButtonOnClick

    fun thighButtonOnClick(view: View) {
        val intent = Intent(this, ThighsExcercisesActivity::class.java)
        startActivity(intent)
    }
    fun calfButtonOnClick(view: View) {
        val intent = Intent(this, CalvesExcercisesActivity::class.java)
        startActivity(intent)
    }
    fun forearmButtonOnClick(view: View) {
        val intent = Intent(this, ForearmsExcercisesActivity::class.java)
        startActivity(intent)
    }
    fun armButtonOnClick(view: View) {
        val intent = Intent(this, ArmsExcercisesActivity::class.java)
        startActivity(intent)
    }
    fun shoulderButtonOnClick(view: View) {
        val intent = Intent(this, ShoulderExcercisesActivity::class.java)
        startActivity(intent)
    }

    fun workoutButtonOnClick(view: View) {
        val intent = Intent(this, WorkoutActivity::class.java)
        startActivity(intent)
    }

    private fun hideButtons(){
        chestOrBackButton = findViewById(R.id.chestOrBackButton)
        chestOrBackButton.setBackgroundColor(Color.TRANSPARENT)

        bellyButton = findViewById(R.id.bellyButton)
        bellyButton.setBackgroundColor(Color.TRANSPARENT)

        leftForearmButton = findViewById(R.id.leftForearmButton)
        leftForearmButton.setBackgroundColor(Color.TRANSPARENT)

        rightForearmButton = findViewById(R.id.rightForearmButton)
        rightForearmButton.setBackgroundColor(Color.TRANSPARENT)

        leftShoulderButton = findViewById(R.id.leftShoulderButton)
        leftShoulderButton.setBackgroundColor(Color.TRANSPARENT)

        rightShoulderButton = findViewById(R.id.rightShoulderButton)
        rightShoulderButton.setBackgroundColor(Color.TRANSPARENT)

        leftArmButton = findViewById(R.id.leftArmButton)
        leftArmButton.setBackgroundColor(Color.TRANSPARENT)

        rightArmButton = findViewById(R.id.rightArmButton)
        rightArmButton.setBackgroundColor(Color.TRANSPARENT)

        leftThighButton = findViewById(R.id.leftThighButton)
        leftThighButton.setBackgroundColor(Color.TRANSPARENT)

        rightThighButton = findViewById(R.id.rightThighButton)
        rightThighButton.setBackgroundColor(Color.TRANSPARENT)

        leftCalfButton = findViewById(R.id.leftCalfButton)
        leftCalfButton.setBackgroundColor(Color.TRANSPARENT)

        rightCalfButton = findViewById(R.id.rightCalfButton)
        rightCalfButton.setBackgroundColor(Color.TRANSPARENT)
    }

}//end of Activity
