
package edu.ib.planner.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import edu.ib.planner.R

class LoginWindowActivity : AppCompatActivity(){


    private var mAuth: FirebaseAuth? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_window)
        mAuth = FirebaseAuth.getInstance()
    }


    fun onClickLogin(view: View?) {
        val edtUsername = findViewById<View>(R.id.username) as EditText
        val edtPassword = findViewById<View>(R.id.password) as EditText
        val email = edtUsername.text.toString()
        val password = edtPassword.text.toString()
        if (email.isEmpty() || password.isEmpty()) {
            Toast.makeText(
                this@LoginWindowActivity.getApplicationContext(), "All fields must be filled",
                Toast.LENGTH_LONG
            ).show()
        } else {
            login()
        }
    }


    private fun login () {
        val edtUsername = findViewById<View>(R.id.username) as EditText
        val edtPassword = findViewById<View>(R.id.password) as EditText
        val email = edtUsername.text.toString()
        val password = edtPassword.text.toString()

        if (!email.isEmpty() && !password.isEmpty()) {
            this.mAuth?.signInWithEmailAndPassword(email, password)
                ?.addOnCompleteListener ( this, OnCompleteListener<AuthResult> { task ->
                    if (task.isSuccessful) {
                        startActivity(Intent(this, MainWindowActivity::class.java))
                        Toast.makeText(this, "Logowanie powiodło się :)", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(this, "Niepoprawny email lub hasło :(", Toast.LENGTH_SHORT).show()
                    }
                })

        }else {
            Toast.makeText(this, "Uzupełnij swoje dane :|", Toast.LENGTH_SHORT).show()
        }
    }

    fun onClickBack(view: View) {

        val intent = Intent(this, StartWindowActivity::class.java)
        startActivity(intent)
    }


}