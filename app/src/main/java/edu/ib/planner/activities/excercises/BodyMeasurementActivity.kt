package edu.ib.planner.activities.excercises

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import edu.ib.planner.R
import edu.ib.planner.activities.LoginWindowActivity
import java.time.LocalDate
import java.util.*

class BodyMeasurementActivity : AppCompatActivity() {

    private val firebaseUser = FirebaseAuth.getInstance().currentUser
    private val db = FirebaseFirestore.getInstance()
    private val TAG = "IBActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_body_measurement)
    }



    @RequiresApi(Build.VERSION_CODES.O)
    fun addToDataBase(view: View) {

            val weightEdit = findViewById<View>(R.id.weight) as EditText
            val waistEdit = findViewById<View>(R.id.waist) as EditText
            val calfEdit = findViewById<View>(R.id.calf) as EditText
            val chestEdit = findViewById<View>(R.id.chest) as EditText
            val thighEdit = findViewById<View>(R.id.thigh) as EditText
            val bicepsEdit = findViewById<View>(R.id.biceps) as EditText
            val hipsEdit = findViewById<View>(R.id.hips) as EditText

            val weight = (weightEdit.text.toString()).toDouble()
            val waist = (waistEdit.text.toString()).toDouble()
            val calf = (calfEdit.text.toString()).toDouble()
            val chest = (chestEdit.text.toString()).toDouble()
            val thigh = (thighEdit.text.toString()).toDouble()
            val biceps = (bicepsEdit.text.toString()).toDouble()
            val hips = (hipsEdit.text.toString()).toDouble()
            val id = (Objects.requireNonNull(firebaseUser)?.uid)


        val bodyMeasurement = hashMapOf(
            "weight" to weight,
            "waist" to waist,
            "calf" to calf,
            "chest" to chest,
            "thigh" to thigh,
            "biceps" to biceps,
            "hips" to hips
        )

        val date = LocalDate.now()

        db.collection("body_measurement").document(id.toString()).collection("body_measurement").document(date.toString())
            .set(bodyMeasurement as Map<String, Any>)
            .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully written!")
                Toast.makeText(this, "Pomiary dodane:)", Toast.LENGTH_LONG).show()}
            .addOnFailureListener { e -> Log.w(TAG, "Error writing document", e) }

}

    fun goToProgressActivity(view: View) {
        val intent = Intent(this, ProgressChartActivity::class.java)
        startActivity(intent)
    }
}