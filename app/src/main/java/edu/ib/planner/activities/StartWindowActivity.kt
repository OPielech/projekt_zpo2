package edu.ib.planner.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import edu.ib.planner.R

class StartWindowActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start_window)
    }//end of onCreate

    fun loginButtonOnClick(view: View) {
        val intent = Intent(this, LoginWindowActivity::class.java)
        startActivity(intent)
    }

    fun registerButtonOnClick(view: View) {
        val intent = Intent(this, RegisterWindowActivity::class.java)
        startActivity(intent)
    }
}
