package edu.ib.planner.activities.excercises

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import edu.ib.planner.R

class WorkoutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_workout)
    }

    fun onClickGoToBodyMeasurement(view: View) {
        val intent = Intent(this, BodyMeasurementActivity::class.java)
        startActivity(intent)
    }
}
