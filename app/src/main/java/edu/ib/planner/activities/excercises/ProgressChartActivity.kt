package edu.ib.planner.activities.excercises

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import edu.ib.planner.R
import java.util.*


class ProgressChartActivity : AppCompatActivity() {

    private val db = FirebaseFirestore.getInstance()
    private val firebaseUser = FirebaseAuth.getInstance().currentUser
    private val id = (Objects.requireNonNull(firebaseUser)?.uid)
    private var collectionReference = db.collection("body_measurement").document(id.toString()).collection("body_measurement")

    var mpLineChart: LineChart? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_progress_chart)

        collectionReference.get()
            .addOnSuccessListener { queryDocumentSnapshots ->

                val weightList = ArrayList<String>()
                val waistList = ArrayList<String>()
                val calfList = ArrayList<String>()
                val chestList = ArrayList<String>()
                val hipsList = ArrayList<String>()
                val bicepsList = ArrayList<String>()
                val thighList = ArrayList<String>()
                for (documentSnapshot in queryDocumentSnapshots) {
                    val weight = documentSnapshot.getDouble("weight")
                    val waist = documentSnapshot.getDouble("waist")
                    val calf = documentSnapshot.getDouble("calf")
                    val chest = documentSnapshot.getDouble("chest")
                    val hips = documentSnapshot.getDouble("hips")
                    val biceps = documentSnapshot.getDouble("biceps")
                    val thigh = documentSnapshot.getDouble("thigh")

                    weightList.add(weight.toString())
                    waistList.add(waist.toString())
                    calfList.add(calf.toString())
                    chestList.add(chest.toString())
                    hipsList.add(hips.toString())
                    bicepsList.add(biceps.toString())
                    thighList.add(thigh.toString())
                }
                val weightValues = ArrayList<Entry>()
                val waistValues = ArrayList<Entry>()
                val calfValues = ArrayList<Entry>()
                val chestValues = ArrayList<Entry>()
                val hipsValues = ArrayList<Entry>()
                val bicepsValues = ArrayList<Entry>()
                val thighValues = ArrayList<Entry>()

                for (i in 0 until weightList.size) {
                    weightValues.add(Entry(i.toFloat(), weightList.get(i).toFloat()))
                    waistValues.add(Entry(i.toFloat(), waistList.get(i).toFloat()))
                    calfValues.add(Entry(i.toFloat(), calfList.get(i).toFloat()))
                    chestValues.add(Entry(i.toFloat(), chestList.get(i).toFloat()))
                    hipsValues.add(Entry(i.toFloat(), hipsList.get(i).toFloat()))
                    bicepsValues.add(Entry(i.toFloat(), bicepsList.get(i).toFloat()))
                    thighValues.add(Entry(i.toFloat(), thighList.get(i).toFloat()))


                }



                mpLineChart = findViewById<View>(R.id.graphid) as LineChart
                val lineDataSet1 = LineDataSet(weightValues, "waga")
                lineDataSet1.setColor(Color.BLUE)
                val lineDataSet2 = LineDataSet(waistValues, "talia")
                lineDataSet2.setColor(Color.MAGENTA)
                val lineDataSet3 = LineDataSet(calfValues, "łydka")
                lineDataSet3.setColor(Color.RED)
                val lineDataSet4 = LineDataSet(chestValues, "klatka piersiowa")
                lineDataSet4.setColor(Color.YELLOW)
                val lineDataSet5 = LineDataSet(bicepsValues, "biceps")
                lineDataSet5.setColor(Color.CYAN)
                val lineDataSet6 = LineDataSet(hipsValues, "biodra")
                lineDataSet6.setColor(Color.DKGRAY)
                val lineDataSet7 = LineDataSet(thighValues, "udo")
                lineDataSet7.setColor(Color.GRAY)
                val dataSets = ArrayList<ILineDataSet>()
                dataSets.add(lineDataSet1)
                dataSets.add(lineDataSet2)
                dataSets.add(lineDataSet3)
                dataSets.add(lineDataSet4)
                dataSets.add(lineDataSet5)
                dataSets.add(lineDataSet6)
                dataSets.add(lineDataSet7)
                val data = LineData(dataSets)
                mpLineChart!!.data = data
                mpLineChart!!.invalidate()


            }.addOnFailureListener {
                Toast.makeText(applicationContext, "ERROR", Toast.LENGTH_LONG).show()
            }



        }





    }




